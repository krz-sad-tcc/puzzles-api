import {
  connection,
  dropDb,
  saveJsonLines,
  setupDb,
} from '@krz-sad-tcc/db-tools';
import { Database } from 'arangojs';
import { Express } from 'express';
import { createApi } from '../src/api';
import { toUrlEncoding } from '../src/services/utils/toUrlEncoding';
import { env } from '../src/env';
import { Advance, PuzzleMainMovesAnswer } from '../src/types';
import request from 'supertest';

global.console.log = jest.fn();
const testDbName = 'apiTest';
let db: Database;
let api: Express;

beforeAll(async () => {
  await setupDb(connection, testDbName, env.DB_USER, env.DB_PASS);
  db = connection.database(testDbName);
  await saveJsonLines(db, 'tests/fixtures/db-data');
  api = createApi({ db });
});

afterAll(async () => {
  await dropDb(connection, testDbName);
});

describe('GET /puzzles/main-moves', () => {
  it('responds with a JSON and a 200 status', async () => {
    const color = 'white';
    const rootEpd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(rootEpd);
    let level;
    let response;

    level = 'easy';

    response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}&rootEpd=${encodedEpd}`,
    );

    expect(response.header['content-type']).toMatch('application/json');
    expect(response.status).toBe(200);

    level = 'medium';

    response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}&rootEpd=${encodedEpd}`,
    );

    expect(response.header['content-type']).toMatch('application/json');
    expect(response.status).toBe(200);

    level = 'hard';

    response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}&rootEpd=${encodedEpd}`,
    );

    expect(response.header['content-type']).toMatch('application/json');
    expect(response.status).toBe(200);
  });

  it('responds with a 422 status when parameters are missing', async () => {
    const color = 'white';
    const rootEpd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(rootEpd);
    const level = 'easy';
    let response;

    response = await request(api).get('/puzzles/main-moves');

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/main-moves?level=${level}&rootEpd=${encodedEpd}`,
    );

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/main-moves?color=${color}&rootEpd=${encodedEpd}`,
    );

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}`,
    );

    expect(response.status).toBe(422);
  });

  it('responds with a 422 status when level is wrong', async () => {
    const color = 'white';
    const rootEpd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(rootEpd);
    const level = 'foo';

    const response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}&rootEpd=${encodedEpd}`,
    );

    expect(response.status).toBe(422);
  });

  it('serializes a position with its respective main moves', async () => {
    const color = 'white';
    const rootEpd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(rootEpd);
    const level = 'hard';

    const response = await request(api).get(
      `/puzzles/main-moves?color=${color}&level=${level}&rootEpd=${encodedEpd}`,
    );

    const { position, answers } = response.body;

    expect(position).toBeInstanceOf(Object);
    expect(Array.isArray(answers)).toBe(true);

    answers.map((answer: PuzzleMainMovesAnswer) => {
      expect(answer).toBeInstanceOf(Object);
      expect(answer.move).toBeInstanceOf(Object);
      expect(answer.position).toBeInstanceOf(Object);
      expect(answer.move._from).toEqual(position._id);
      expect(answer.move._to).toEqual(answer.position._id);
    });
  });
});

describe('GET /puzzles/survival', () => {
  it('responds with a JSON and a 200 status', async () => {
    const computer = false;
    const epd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(epd);
    const level = 'easy';

    const response = await request(api).get(
      `/puzzles/survival?computer=${computer}&epd=${encodedEpd}&level=${level}`,
    );

    expect(response.header['content-type']).toMatch('application/json');
    expect(response.status).toBe(200);
  });

  it('responds with a 422 status when parameters are missing', async () => {
    const computer = false;
    const epd = 'rnbqkb1r/pppppppp/5n2/8/3P4/8/PPP1PPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(epd);
    const level = 'easy';

    let response;

    response = await request(api).get('/puzzles/survival');

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/survival?computer=${computer}&epd=${encodedEpd}`,
    );

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/survival?computer=${computer}&level=${level}`,
    );

    expect(response.status).toBe(422);

    response = await request(api).get(
      `/puzzles/survival?epd=${encodedEpd}&level=${level}`,
    );

    expect(response.status).toBe(422);
  });

  it("doesn't give a computer advance when not requested", async () => {
    const computer = false;
    const epd = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(epd);
    const level = 'easy';

    const response = await request(api).get(
      `/puzzles/survival?computer=${computer}&epd=${encodedEpd}&level=${level}`,
    );

    expect(response.body.computerAdvance).toBe(null);
    expect(response.body.playerAdvances).toBeInstanceOf(Object);
  });

  it('gives a computer advance when requested', async () => {
    const computer = true;
    const epd = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(epd);
    const level = 'easy';

    const response = await request(api).get(
      `/puzzles/survival?computer=${computer}&epd=${encodedEpd}&level=${level}`,
    );

    expect(response.body.computerAdvance).toBeInstanceOf(Object);
    expect(response.body.playerAdvances).toBeInstanceOf(Object);
  });

  it('provides an array of advances for the player', async () => {
    const computer = false;
    const epd = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -';
    const encodedEpd = toUrlEncoding(epd);
    const level = 'easy';

    const response = await request(api).get(
      `/puzzles/survival?computer=${computer}&epd=${encodedEpd}&level=${level}`,
    );

    expect(response.body.playerAdvances.length).toBeGreaterThan(0);
    expect(
      response.body.playerAdvances.map((a: Advance) => a.move.moveSan),
    ).toEqual(expect.arrayContaining(['e4', 'd4']));
  });
});

describe('GET /positions/search', () => {
  it('responds with a JSON and a 200 status', async () => {
    const term = 'french';

    const response = await request(api).get(`/positions/search?term=${term}`);

    expect(response.header['content-type']).toMatch('application/json');
    expect(response.status).toBe(200);
  });

  it('responds with a 422 status when parameters are missing', async () => {
    let response;

    response = await request(api).get('/positions/search?term=');

    expect(response.status).toBe(422);

    response = await request(api).get('/positions/search');

    expect(response.status).toBe(422);
  });

  it('finds a position whose name matches the given term', async () => {
    const term = 'french';

    const response = await request(api).get(`/positions/search?term=${term}`);

    const { positions } = response.body;

    expect(positions).not.toHaveLength(0);
    expect(positions[0].name).toBe('French Defense');
  });
});
