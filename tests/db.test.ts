import {
  connection,
  dropDb,
  saveJsonLines,
  setupDb,
} from '@krz-sad-tcc/db-tools';
import { Database } from 'arangojs';
import {
  getMovesFromPosition,
  getPositionByEpd,
  getPositionByKey,
  getPositionByName,
  getMainPathToPosition,
  getNamedParent,
  searchPosition,
} from '../src/db';
import { env } from '../src/env';

global.console.log = jest.fn();
const testDbName = 'dbTest';
let db: Database;

beforeAll(async () => {
  await setupDb(connection, testDbName, env.DB_USER, env.DB_PASS);
  db = connection.database(testDbName);
  await saveJsonLines(db, 'tests/fixtures/db-data');
});

afterAll(async () => {
  await dropDb(connection, testDbName);
});

describe('#getMovesFromPosition', () => {
  it('returns all the moves for a given position', async () => {
    const initialPosition = await getPositionByName(db, 'Initial Position');
    const result = await getMovesFromPosition(db, initialPosition._key);

    const expectedMoves = [
      'e4',
      'd4',
      'Nf3',
      'c4',
      'g3',
      'b3',
      'f4',
      'Nc3',
      'b4',
      'e3',
      'd3',
      'a3',
    ];

    expect(result.length).toBe(12);
    expect(result.every((move, i) => move.moveSan == expectedMoves[i])).toBe(
      true,
    );
  });
});

describe('#getPositionByEpd', () => {
  it('returns the position that has the given fen', async () => {
    const referencePosition = await getPositionByName(db, 'French Defense');
    const result = await getPositionByEpd(db, referencePosition.epd);

    expect(result).toMatchObject(referencePosition);
  });
});

describe('#getPositionByKey', () => {
  it('returns the position that has the given key', async () => {
    const referencePosition = await getPositionByName(db, 'French Defense');
    const result = await getPositionByKey(db, referencePosition._key);

    expect(result).toMatchObject(referencePosition);
  });
});

describe('#getPositionByName', () => {
  it('returns a position that has the given name', async () => {
    const result = await getPositionByName(db, 'French Defense');

    expect(result.name).toBe('French Defense');
  });
});

describe('#getMainPathToPosition', () => {
  it('returns an array with the main moves leading to the position', async () => {
    // Queen's Indian Defense: d4, Nf6, c4, e6, Nf3, b6.
    const referenceEpd =
      'rnbqkb1r/p1pp1ppp/1p2pn2/8/2PP4/5N2/PP2PPPP/RNBQKB1R w KQkq -';
    const expectedMoves = ['d4', 'Nf6', 'c4', 'e6', 'Nf3', 'b6'];

    const referencePosition = await getPositionByEpd(db, referenceEpd);
    const result = await getMainPathToPosition(db, referencePosition._id);

    expect(result.edges).toHaveLength(6);
    expect(
      result.edges.every((move, i) => move.moveSan == expectedMoves[i]),
    ).toBe(true);
  });
});

describe('#getNamedParent', () => {
  it('returns the main parent of a given position', async () => {
    // A position with 4 candidate parents.
    const referenceEpd =
      'rnbqkb1r/ppp2ppp/4pn2/3p4/2PP4/P4N2/1P2PPPP/RNBQKB1R b KQkq -';
    const referencePosition = await getPositionByEpd(db, referenceEpd);

    const expectedParentEpd =
      'rnbqkb1r/ppp1pppp/5n2/3p4/3P4/5N2/PPP1PPPP/RNBQKB1R w KQkq -';
    const expectedParent = await getPositionByEpd(db, expectedParentEpd);

    const result = await getNamedParent(db, referencePosition._id);

    expect(result).toMatchObject(expectedParent);
  });
});

describe('searchPosition', () => {
  it('finds a position matching the given string at the beginning', async () => {
    const term = 'Ruy';
    const result = await searchPosition(db, term);
    expect(result.some((pos) => pos.name === 'Ruy Lopez'));
  });

  it('finds a position matching the given string in the middle', async () => {
    const term = 'Lopez';
    const result = await searchPosition(db, term);
    expect(result.some((pos) => pos.name === 'Ruy Lopez'));
  });

  it('finds a position matching the given string at the end', async () => {
    const term = 'y L';
    const result = await searchPosition(db, term);
    expect(result.some((pos) => pos.name === 'Ruy Lopez'));
  });

  it('ignores case when matching strings', async () => {
    const termAllUpperCase = 'RUY LOPEZ';
    const result1 = await searchPosition(db, termAllUpperCase);
    expect(result1.some((pos) => pos.name === 'Ruy Lopez'));

    const termAllLowerCase = 'ruy lopez';
    const result2 = await searchPosition(db, termAllLowerCase);
    expect(result2.some((pos) => pos.name === 'Ruy Lopez'));
  });
});
