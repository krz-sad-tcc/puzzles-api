export const Coverage = {
  LOW: 0.5,
  REGULAR: 0.92,
} as const;

export enum Distribution {
  ByNumberOfGames,
  Uniform,
}
