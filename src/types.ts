/**
 * Represents the colors of the pieces.
 */
export type Color = 'black' | 'white';

/**
 * Represents the colors of the pieces as encoded in FEN.
 */
export type FenColor = 'b' | 'w';

/**
 * Represents the level of difficulty of a puzzle.
 */
export type DifficultyLevel = 'easy' | 'medium' | 'hard';

/**
 * Describes Position model in the database.
 */
export interface Position {
  eco: string;
  epd: string;
  fen: string;
  isLeaf: boolean;
  name: string;
  nGames: number;
  parentEco?: string;
  parentName?: string;
}

/**
 * Describes LeadsTo model in the database.
 */
export interface Move {
  _from: string;
  _to: string;
  moveSan: string;
  nGames: number;
}

/**
 * Represents an actual document saved in the database, including fields
 * that are automatically added by arangodb.
 */
export type ArangoRecord<T> = T & {
  _key: string;
  _rev: string;
  _id: string;
};

/**
 * Represents an advance in a chess board situation.
 * Comprises a move along with its following position.
 */
export type Advance = {
  move: ArangoRecord<Move>;
  position: ArangoRecord<Position>;
};

/**
 * A sequence of advances.
 */
export type History = {
  advances: Advance[];
};

/**
 * Represents a single answer to a Main Moves puzzle.
 */
export type PuzzleMainMovesAnswer = Advance;

/**
 * Represents the complete information needed by a Main Moves puzzle.
 * I.e., a position and an array of answers.
 */
export type PuzzleMainMoves = {
  position: ArangoRecord<Position>;
  answers: PuzzleMainMovesAnswer[];
};

/**
 * Represents a step in a Survival puzzle.
 * It may contain an advance for the computer
 * and candidate advances for the player. Whenever one of them
 * does not exist, null is used instead.
 */
export type PuzzleSurvivalStep = {
  computerAdvance: Advance | null;
  playerAdvances: Advance[] | null;
};
