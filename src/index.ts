import { connection } from '@krz-sad-tcc/db-tools';
import { createApi } from './api';
import { env } from './env';

const main = () => {
  const db = connection.database(env.DB_NAME);
  const api = createApi({ db });

  api.listen(env.API_PORT, () => {
    console.log(`Listening on port ${env.API_PORT}`);
  });
};

main();
