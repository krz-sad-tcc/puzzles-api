import dotenv from 'dotenv';

dotenv.config();

type NodeEnv = 'development' | 'production' | 'staging' | 'test';

export const env = {
  NODE_ENV: (process.env.NODE_ENV || 'development') as NodeEnv,
  DB_HOST: process.env.DB_HOST || '',
  DB_NAME: process.env.DB_NAME || '',
  DB_USER: process.env.DB_USER || '',
  DB_PASS: process.env.DB_PASS || '',
  CORS_ORIGINS: process.env.CORS_ORIGINS || '',
  API_PORT: parseInt(process.env.API_PORT || ''),
};
