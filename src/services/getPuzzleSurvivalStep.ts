import { Database } from 'arangojs';
import { Coverage, Distribution } from '../constants/puzzle';
import {
  getMovesFromPosition,
  getNamedParent,
  getPositionByEpd,
  getPositionByKey,
} from '../db';
import { DifficultyLevel, PuzzleSurvivalStep } from '../types';
import { getMoveProbabilistically } from './utils/getMoveProbabilistically';
import { getRelevantMoves } from './utils/getRelevantMoves';

export const getPuzzleSurvivalStep = async (
  db: Database,
  computer: boolean,
  epd: string,
  level: DifficultyLevel,
): Promise<PuzzleSurvivalStep> => {
  let candidateMoves, namedParent, nextPositionKey, nextPosition;
  let computerAdvance = null;
  let playerAdvances = null;

  const coverage = level === 'easy' ? Coverage.LOW : Coverage.REGULAR;
  const distribution =
    level === 'hard' ? Distribution.Uniform : Distribution.ByNumberOfGames;

  const basePosition = await getPositionByEpd(db, epd);
  candidateMoves = await getMovesFromPosition(db, basePosition._key);

  if (computer) {
    candidateMoves = getRelevantMoves(basePosition, candidateMoves, coverage);

    if (candidateMoves.length === 0) {
      return {
        computerAdvance,
        playerAdvances,
      };
    }

    const computerMove = getMoveProbabilistically(distribution, candidateMoves);

    nextPositionKey = computerMove._to.split('/')[1];
    const positionAfterComputer = await getPositionByKey(db, nextPositionKey);

    if (!positionAfterComputer.name) {
      namedParent = await getNamedParent(db, positionAfterComputer._id);
      positionAfterComputer.parentEco = namedParent.eco;
      positionAfterComputer.parentName = namedParent.name;
    }

    computerAdvance = {
      move: computerMove,
      position: positionAfterComputer,
    };

    candidateMoves = await getMovesFromPosition(db, positionAfterComputer._key);
  }

  if (candidateMoves.length === 0) {
    return {
      computerAdvance,
      playerAdvances,
    };
  }

  const playerMoves = getRelevantMoves(
    computerAdvance?.position || basePosition,
    candidateMoves,
    coverage,
  );

  playerAdvances = [];

  for (let i = 0; i < playerMoves.length; i++) {
    const move = playerMoves[i];
    nextPositionKey = move._to.split('/')[1];
    nextPosition = await getPositionByKey(db, nextPositionKey);

    if (!nextPosition.name) {
      namedParent = await getNamedParent(db, nextPosition._id);
      nextPosition.parentEco = namedParent.eco;
      nextPosition.parentName = namedParent.name;
    }

    playerAdvances.push({
      move,
      position: nextPosition,
    });
  }

  return {
    computerAdvance,
    playerAdvances,
  };
};
