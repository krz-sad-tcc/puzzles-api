import { Database } from 'arangojs';
import { getMainPathToPosition, getPositionByEpd } from '../db';
import { Advance, History } from '../types';

export const getHistory = async (
  db: Database,
  epd: string,
): Promise<History> => {
  const advances: Advance[] = [];

  const position = await getPositionByEpd(db, epd);
  const { vertices, edges } = await getMainPathToPosition(db, position._id);

  if (!vertices || !edges) {
    return { advances };
  }

  for (let i = 1; i < vertices.length; i++) {
    advances.push({
      move: edges[i - 1],
      position: vertices[i],
    });
  }

  return {
    advances,
  };
};
