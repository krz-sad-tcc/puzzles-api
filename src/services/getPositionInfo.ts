import { Database } from 'arangojs';
import { getMainPathToPosition } from '../db';
import { ArangoRecord, Move } from '../types';
import fetch from 'node-fetch';

type WikibooksResponse = {
  query: {
    pages: {
      missing: boolean;
      invalid: boolean;
      invalidreason: boolean;
      extract: string;
    }[];
  };
};

const cleanHTMLContent = (html: string) => {
  const removeTitle = (html: string) =>
    html.replace(/<span id=".+">.+<\/span>/g, '');
  const removeEmptyParagraph = (html: string) =>
    html.replace(/<p>(<br \/>|\s)*<\/p>/g, '');
  const removeTableHeader = (html: string) =>
    html.replace('<h2><span id="Theory_table">Theory table</span></h2>', '');
  const removeTableExpl = (html: string) =>
    html.replace(
      /For explanation of theory tables see theory table and for notation see algebraic notation.?/,
      '',
    );
  const removeContributing = (html: string) =>
    html.replace(
      'When contributing to this Wikibook, please follow the Conventions for organization.',
      '',
    );

  return removeEmptyParagraph(
    removeTitle(removeTableHeader(removeTableExpl(removeContributing(html)))),
  );
};

const findOnWikibooks = async (pgn: string): Promise<string | null> => {
  const wikiBooksUrl = 'https://en.wikibooks.org';
  const apiArgs =
    'redirects&origin=*&action=query&prop=extracts&formatversion=2&format=json';

  const title = `Chess_Opening_Theory/${pgn}`;

  const res = await fetch(
    `${wikiBooksUrl}/w/api.php?titles=${title}&${apiArgs}`,
  );
  if (!res.ok) {
    return null;
  }

  const json = (await res.json()) as WikibooksResponse;
  const page = json.query.pages[0];

  if (page.missing) {
    console.log('*** missing page: ', pgn);
    return null;
  } else if (page.invalid) {
    console.log('*** invalid request: ', pgn, page.invalidreason);
    return null;
  } else if (!page.extract) {
    console.log('*** unexpected api response', pgn, JSON.stringify(page));
    return null;
  }
  return cleanHTMLContent(page.extract);
};

const buildPGN = (path: ArangoRecord<Move>[]): string => {
  if (path.length === 0) {
    return '';
  }

  const pgn = [];
  for (let i = 0; i < path.length; i += 2) {
    const turn = Math.floor(i / 2);

    pgn.push(`${turn + 1}. ${path[i].moveSan}`);

    if (i + 1 < path.length) {
      pgn.push(`${turn + 1}...${path[i + 1].moveSan}`);
    }
  }

  return pgn.join('/');
};

export const getPositionInfo = async (
  db: Database,
  key: string,
): Promise<string | null> => {
  const path = await getMainPathToPosition(db, key);
  const pgn = buildPGN(path.edges);

  if (!pgn) return null;

  return findOnWikibooks(pgn);
};
