import { Database } from 'arangojs';
import { hasGrandchildren, searchPosition } from '../db';

export const getPositionsByTerm = async (db: Database, term: string) => {
  const matchedPositions = await searchPosition(db, term);
  const truthArray = await Promise.all(
    matchedPositions.map(async (p) => hasGrandchildren(db, p)),
  );
  return matchedPositions.filter((_p, index) => truthArray[index]);
};
