import { Database } from 'arangojs';
import { Coverage, Distribution } from '../constants/puzzle';
import {
  getMovesFromPosition,
  getNamedParent,
  getPositionByEpd,
  getPositionByKey,
} from '../db';
import {
  ArangoRecord,
  Color,
  DifficultyLevel,
  Position,
  PuzzleMainMoves,
  PuzzleMainMovesAnswer,
} from '../types';
import { getPathProbabilistically } from './utils/getPathProbabilistically';
import { getRelevantMoves } from './utils/getRelevantMoves';

export const getMainMovesPuzzle = async (
  db: Database,
  color: Color,
  level: DifficultyLevel,
  rootEpd: string,
): Promise<PuzzleMainMoves> => {
  const rootPosition = await getPositionByEpd(db, rootEpd);
  const reducedColor = color === 'white' ? 'w' : 'b';
  let maxGames: number, minGames: number;

  const nGamesInterval = rootPosition.nGames - 100;

  switch (level) {
    case 'easy':
      maxGames = 100 + nGamesInterval;
      minGames = 100 + Math.floor((1 / 250) * nGamesInterval);
      break;
    case 'medium':
      maxGames = 100 + Math.ceil((1 / 250) * nGamesInterval);
      minGames = 100 + Math.floor((1 / 1500) * nGamesInterval);
      break;
    case 'hard':
      maxGames = 100 + Math.ceil((1 / 1500) * nGamesInterval);
      minGames = 100;
      break;
  }

  let path: ArangoRecord<Position>[] = [];
  let candidates: ArangoRecord<Position>[] = [];

  function getCandidates(
    path: ArangoRecord<Position>[],
    color: 'w' | 'b',
  ): ArangoRecord<Position>[] {
    return path.filter(
      (p) =>
        p.fen.split(' ')[1] === color &&
        p.nGames <= maxGames &&
        p.nGames >= minGames,
    );
  }

  candidates = getCandidates(path, reducedColor);

  let counter = 0;
  while (candidates.length === 0) {
    counter++;

    if (counter > 25) {
      candidates.push(rootPosition);
      break;
    }

    path = await getPathProbabilistically(
      db,
      Distribution.ByNumberOfGames,
      rootPosition,
    );
    candidates = getCandidates(path, reducedColor);
  }

  // Generate a random number in the interval [0, candidates.length[.
  const i = Math.floor(Math.random() * candidates.length);

  const basePosition = candidates[i];

  const moves = await getMovesFromPosition(db, basePosition._key);
  const relevantMoves = getRelevantMoves(basePosition, moves, Coverage.REGULAR);

  if (!basePosition.name) {
    const namedParent = await getNamedParent(db, basePosition._id);
    basePosition.parentEco = namedParent.eco;
    basePosition.parentName = namedParent.name;
  }

  const answers: PuzzleMainMovesAnswer[] = [];
  let resultingPosition;

  for (const move of relevantMoves) {
    resultingPosition = await getPositionByKey(db, move._to.split('/')[1]);
    answers.push({
      move,
      position: resultingPosition,
    });
  }

  return {
    position: basePosition,
    answers,
  };
};
