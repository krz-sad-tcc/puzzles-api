import { ArangoRecord, Move, Position } from '../../types';

export const getRelevantMoves = (
  basePosition: ArangoRecord<Position>,
  moves: ArangoRecord<Move>[],
  relevance: number,
): ArangoRecord<Move>[] => {
  const maxGames = relevance * basePosition.nGames;
  const relevantMoves: ArangoRecord<Move>[] = [];

  moves.sort((a, b) => b.nGames - a.nGames);

  let sum = 0;

  for (let i = 0; i < moves.length; i++) {
    const move = moves[i];

    sum += move.nGames;
    relevantMoves.push(move);

    if (sum > maxGames) break;
  }

  return relevantMoves;
};
