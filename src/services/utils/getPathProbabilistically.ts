import { Database } from 'arangojs';
import { Distribution } from '../../constants/puzzle';
import { getMovesFromPosition, getPositionByKey } from '../../db';
import { ArangoRecord, Position } from '../../types';
import { getMoveProbabilistically } from './getMoveProbabilistically';

export const getPathProbabilistically = async (
  db: Database,
  distribution: Distribution,
  root: ArangoRecord<Position>,
): Promise<ArangoRecord<Position>[]> => {
  const path: ArangoRecord<Position>[] = [];
  let currentPosition = root;
  let currentMove;
  let moves = await getMovesFromPosition(db, currentPosition._key);

  while (moves.length > 0) {
    currentMove = getMoveProbabilistically(distribution, moves);
    currentPosition = await getPositionByKey(db, currentMove._to.split('/')[1]);
    path.push(currentPosition);
    moves = await getMovesFromPosition(db, currentPosition._key);
  }

  return path;
};
