import { Color, FenColor } from '../../types';

export const doesFenMatchColor = (fen: string, color: Color): boolean => {
  const fenColor: FenColor = fen.split(' ')[1] as FenColor;

  if (fenColor === ('b' as FenColor)) {
    return color === ('black' as Color);
  }
  return color === ('white' as Color);
};
