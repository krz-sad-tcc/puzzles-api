import { Distribution } from '../../constants/puzzle';
import { ArangoRecord, Move } from '../../types';

export const getMoveProbabilistically = (
  distribution: Distribution,
  moves: ArangoRecord<Move>[],
): ArangoRecord<Move> => {
  const nGames = moves.map((move) => move.nGames).reduce((a, b) => a + b, 0);

  if (distribution === Distribution.ByNumberOfGames) {
    // Generate a random number in the interval [1, nGames].
    const pivot = Math.random() * nGames + 1;

    let sum = 0;
    for (let i = 0; i < moves.length; i++) {
      sum += moves[i].nGames;

      if (sum >= pivot) {
        return moves[i];
      }
    }
  } else if (distribution === Distribution.Uniform) {
    // Generate a random integer in the interval [0, moves.length[.
    const i = Math.floor(Math.random() * moves.length);
    return moves[i];
  }

  // Safety return.
  return moves[0];
};
