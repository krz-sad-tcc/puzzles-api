export const toUrlEncoding = (str: string) => {
  return str.replace(/ /g, '%20').replace(/\//g, '%2F');
};
