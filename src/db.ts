import { Database } from 'arangojs';
import {
  COLL_POSITIONS,
  GRAPH,
  query,
  withErrorHandling,
} from '@krz-sad-tcc/db-tools';
import { ArangoRecord, Move, Position } from './types';

export const getMovesFromPosition = async (db: Database, key: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Move>>(db, {
      query: `
        FOR v, e IN 1..1 OUTBOUND @id GRAPH @graph
          RETURN e
      `,
      bindVars: {
        graph: GRAPH,
        id: `${COLL_POSITIONS}/${key}`,
      },
    });
    return result;
  });
};

export const getPositionByEpd = async (db: Database, epd: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        FOR p IN @@collection
          FILTER p.epd == @epd
          LIMIT 1
          RETURN p
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        epd: epd,
      },
    });
    return result[0];
  });
};

export const getPositionByKey = async (db: Database, key: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        FOR p IN @@collection
          FILTER p._key == @key
          LIMIT 1
          RETURN p
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        key: key,
      },
    });
    return result[0];
  });
};

export const getPositionByName = async (db: Database, name: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        LET candidates = (
          FOR p IN @@collection
            FILTER LIKE(p.name, @name)
            SORT p.nGames
            RETURN p
        )
        RETURN LAST(candidates)
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        name: name,
      },
    });
    return result[0];
  });
};

export const getMainPathToPosition = async (db: Database, id: string) => {
  return withErrorHandling(async () => {
    const result = await query<{
      edges: ArangoRecord<Move>[];
      vertices: ArangoRecord<Position>[];
    }>(db, {
      query: `
        LET paths = (
          FOR v, e, p IN 1..999 INBOUND @id GRAPH @graph
            PRUNE condition = v.name == "Initial Position"
            OPTIONS { order: "weighted", weightAttribute: "nGames" }
            FILTER condition
            RETURN p
        )
        RETURN {
          edges: REVERSE(LAST(paths).edges),
          vertices: REVERSE(LAST(paths).vertices)
        }
      `,
      bindVars: {
        graph: GRAPH,
        id: id,
      },
    });
    return result[0];
  });
};

export const getNamedParent = async (db: Database, id: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        LET vertices = (
          FOR v IN 1..999 INBOUND @id GRAPH @graph
            PRUNE condition = v.name != ""
            OPTIONS { order: "weighted", weightAttribute: "nGames" }
            FILTER condition
            RETURN v
        )
        RETURN LAST(vertices)
      `,
      bindVars: {
        graph: GRAPH,
        id: id,
      },
    });
    return result[0];
  });
};

export const searchPosition = async (db: Database, term: string) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        LET distinct_positions = (
          FOR p IN @@collection
            FILTER LIKE(p.name, @search_expression, true)
            SORT p.nGames DESC
            COLLECT name = p.name INTO positions_by_name
            RETURN FIRST(positions_by_name).p
        )
        FOR p IN distinct_positions
          SORT p.nGames DESC
          RETURN p
      `,
      bindVars: {
        '@collection': COLL_POSITIONS,
        search_expression: `%${term}%`,
      },
    });
    return result;
  });
};

export const hasGrandchildren = async (
  db: Database,
  position: ArangoRecord<Position>,
) => {
  return withErrorHandling(async () => {
    const result = await query<ArangoRecord<Position>>(db, {
      query: `
        FOR v IN 2..2 OUTBOUND @id GRAPH @graph
          RETURN v
      `,
      bindVars: {
        graph: GRAPH,
        id: position._id,
      },
    });
    return result.length > 0;
  });
};
