import { env } from './env';
import { Database } from 'arangojs/database';
import { getPositionByEpd } from './db';
import { getHistory } from './services/getHistory';
import { getMainMovesPuzzle } from './services/getPuzzleMainMoves';
import { getPositionInfo } from './services/getPositionInfo';
import { getPositionsByTerm } from './services/getPositionsByTerm';
import { getPuzzleSurvivalStep } from './services/getPuzzleSurvivalStep';
import {
  ArangoRecord,
  Color,
  DifficultyLevel,
  History,
  Position,
  PuzzleMainMoves,
  PuzzleSurvivalStep,
} from './types';
import cors from 'cors';
import express, { Response, Request } from 'express';
import helmet from 'helmet';
import morgan from 'morgan';

interface ApiContext {
  db: Database;
}

export const createApi = ({ db }: ApiContext) => {
  const app = express();

  const options: cors.CorsOptions = {
    origin: env.CORS_ORIGINS.split(','),
  };

  app.use(cors(options));
  app.use(helmet());

  if (env.NODE_ENV !== 'test') {
    app.use(morgan('combined'));
  }

  app.get(
    '/puzzles/main-moves',
    async (
      req: Request<
        unknown,
        unknown,
        unknown,
        {
          color: Color;
          level: DifficultyLevel;
          rootEpd: string;
        }
      >,
      res: Response<PuzzleMainMoves>,
    ) => {
      if (!req.query.color || !req.query.level || !req.query.rootEpd) {
        return res.status(422).send();
      }

      const color = req.query.color;

      if (!['black', 'white'].includes(color)) {
        return res.status(422).send();
      }

      const level = req.query.level;

      if (!['easy', 'medium', 'hard'].includes(level)) {
        return res.status(422).send();
      }

      const puzzle = await getMainMovesPuzzle(
        db,
        color,
        level,
        req.query.rootEpd,
      );

      res.json(puzzle);
    },
  );

  app.get(
    '/puzzles/survival',
    async (
      req: Request<
        unknown,
        unknown,
        unknown,
        { computer: string; epd: string; level: DifficultyLevel }
      >,
      res: Response<PuzzleSurvivalStep>,
    ) => {
      if (!req.query.computer || !req.query.epd || !req.query.level) {
        return res.status(422).send();
      }

      const computer = JSON.parse(req.query.computer);
      const epd = req.query.epd;
      const level = req.query.level;

      if (!['easy', 'medium', 'hard'].includes(level)) {
        return res.status(422).send();
      }

      const puzzleStep = await getPuzzleSurvivalStep(db, computer, epd, level);

      res.json(puzzleStep);
    },
  );

  app.get(
    '/positions',
    async (
      req: Request<unknown, unknown, unknown, { epd: string }>,
      res: Response<ArangoRecord<Position>>,
    ) => {
      if (!req.query.epd) {
        return res.status(422).send();
      }
      const epd = req.query.epd;
      const position = await getPositionByEpd(db, epd);

      res.json(position);
    },
  );

  app.get(
    '/positions/history',
    async (
      req: Request<unknown, unknown, unknown, { epd: string }>,
      res: Response<History>,
    ) => {
      if (!req.query.epd) {
        return res.status(422).send();
      }
      const epd = req.query.epd;
      const history = await getHistory(db, epd);

      res.json(history);
    },
  );

  app.get(
    '/positions/search',
    async (
      req: Request<unknown, unknown, unknown, { term: string }>,
      res: Response<{ positions: ArangoRecord<Position>[] }>,
    ) => {
      if (!req.query.term) {
        return res.status(422).send();
      }
      const term = req.query.term;
      const positions = await getPositionsByTerm(db, term);

      res.json({ positions });
    },
  );

  app.get(
    '/positions/:key/info',
    async (req: Request, res: Response<{ info: string | null }>) => {
      if (!req.params.key) {
        return res.status(422).send();
      }
      const key = req.params.key;
      const info = await getPositionInfo(db, key);

      res.json({ info });
    },
  );

  return app;
};
