# Opening Puzzles API

This is the server side of **Opening Puzzles**, a web application whose purpose is to allow users to practice chess openings. It is part of the CS graduation [final project](https://linux.ime.usp.br/~renankrz/) by Renan Krzesinski and Ygor Sad Machado.

## Technologies

- Engine: Node.js
- Back end framework: Express
- Testing framework: Jest

## Requirements

- Opening Puzzles graph database. Follow instructions [here](https://gitlab.com/krz-sad-tcc/graph-builder)
- Node.js 14

We recommend the use of [NVM](https://github.com/nvm-sh/nvm) to manage Node.js versions.

## Dev

Clone the project, copy the file `.env.example` and name the new file `.env`. Modify it according to your needs. Then run

```bash
yarn run dev
```

The server should now be listening on 1234 or whichever port you specified in `.env`. Other scripts available in `package.json`.
