#!/bin/bash

PEM_KEY=$1
if [ -z "$PEM_KEY" ]; then
  echo "Please provide path to your pem key" && exit 1
fi

SERVER_USER=admin
SERVER_URI=stilltheory.org
DEPLOY_PATH="~/app/api"
FILES_TO_COPY="src package.json yarn.lock tsconfig.json"

ssh -i $PEM_KEY $SERVER_USER@$SERVER_URI "rm -rf $DEPLOY_PATH/*"
scp -i $PEM_KEY -r $FILES_TO_COPY $SERVER_USER@$SERVER_URI:$DEPLOY_PATH
ssh -i $PEM_KEY $SERVER_USER@$SERVER_URI \
  "cd $DEPLOY_PATH; \
   yarn install; \
   pm2 restart api;"
